import { Injectable,Scope } from '@nestjs/common';

@Injectable({scope:Scope.REQUEST})  // this is to specify injection scope in options object
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}
// singleton scope is used by default  // check controller