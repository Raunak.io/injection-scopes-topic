import { Controller, Get, Scope } from '@nestjs/common';
import { AppService } from './app.service';

@Controller({
  path:'',
  scope:Scope.REQUEST, // BY THIS A NEW INSTANCE IS CREATED IN EVERY REQUEST
})
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
// CHECK APP SERVICE 
// USING REQUEST SCOPED PROVIDERS CAN AFFECT THE APP PERFORMANCE COZ OF CREATING NEW INSTANCES